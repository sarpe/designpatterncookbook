package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.Singleton.Singleton;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 01/05/2015.
 */
public class SingletonTest {

    @Test
    public void testSingleton() {
        System.out.println("\nSingleton Pattern\n");

        Singleton single1 = Singleton.getInstance();
        Singleton single2 = Singleton.getInstance();

        Assert.assertTrue("Singleton object reference is not the same", single1 == single2);

    }
}

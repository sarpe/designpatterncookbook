package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.Strategy.DecoyDuck;
import com.arpe.simon.designpatterncookbook.Strategy.Duck;
import com.arpe.simon.designpatterncookbook.Strategy.MallardDuck;
import com.arpe.simon.designpatterncookbook.Strategy.RedheadDuck;
import com.arpe.simon.designpatterncookbook.Strategy.RubberDuck;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 30/04/2015.
 */
public class StrategyTest {

    @Test
    public void testStrategy() {
        System.out.println("\nStrategy Pattern\n");

        Duck duck;
        duck = new MallardDuck();
        duck.display();
        duck.performFly();
        duck.performQuack();

        Assert.assertTrue("duck object is not an instance of MallardDuck", duck instanceof MallardDuck);

        duck = new RedheadDuck();
        duck.display();
        duck.performFly();
        duck.performQuack();
        duck = new RubberDuck();
        duck.display();
        duck.performFly();
        duck.performQuack();
        duck = new DecoyDuck();
        duck.display();
        duck.performFly();
        duck.performQuack();
        Assert.assertTrue("duck object is not an instance of DecoyDuck", duck instanceof DecoyDuck);

    }
}

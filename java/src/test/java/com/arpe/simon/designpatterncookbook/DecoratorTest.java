package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.Decorator.Beverage;
import com.arpe.simon.designpatterncookbook.Decorator.Espresso;
import com.arpe.simon.designpatterncookbook.Decorator.HouseBlend;
import com.arpe.simon.designpatterncookbook.Decorator.Mocha;
import com.arpe.simon.designpatterncookbook.Decorator.Whip;


import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 30/04/2015.
 */
public class DecoratorTest {

    @Test
    public void testDecorator() {
        System.out.println("\nDecorator Patter\n");
        Beverage beverage = new Espresso();

        System.out.println(String.format("%s $ %.2f", beverage.getDescription(), beverage.cost()));
        Assert.assertTrue("Espresso name doesn't match", "Espresso".equals(beverage.getDescription()));
        Assert.assertTrue("Espresso cost doesn't match", 1.99 == beverage.cost());

        Beverage beverage1 = new HouseBlend();
        beverage1 = new Mocha(beverage1);
        beverage1 = new Whip(beverage1);

        System.out.println(String.format("%s $ %.2f", beverage1.getDescription(), beverage1.cost()));
        Assert.assertTrue("HouseBlend name doesn't match", "House Blend Coffee, Mocha, Whip".equals(beverage1.getDescription()));
        Assert.assertTrue("HouseBlend Mocha Whip cost doesn't match", "2.47".equals(String.format("%.2f", beverage1.cost())));


    }
}

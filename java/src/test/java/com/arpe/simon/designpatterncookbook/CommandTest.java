package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.Command.Garage;
import com.arpe.simon.designpatterncookbook.Command.GarageOffCommand;
import com.arpe.simon.designpatterncookbook.Command.GarageOnCommand;
import com.arpe.simon.designpatterncookbook.Command.Light;
import com.arpe.simon.designpatterncookbook.Command.LightOffCommand;
import com.arpe.simon.designpatterncookbook.Command.LightOnCommand;
import com.arpe.simon.designpatterncookbook.Command.RemoteControl;
import com.arpe.simon.designpatterncookbook.Command.Stereo;
import com.arpe.simon.designpatterncookbook.Command.StereoOffCommand;
import com.arpe.simon.designpatterncookbook.Command.StereoOnCommand;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 01/05/2015.
 */
public class CommandTest {

    @Test
    public void testCommand() {
        System.out.print("\nCommand Pattern\n");

        RemoteControl remote = new RemoteControl();

        Light light = new Light();
        LightOnCommand lightOn = new LightOnCommand(light);
        LightOffCommand lightOff = new LightOffCommand(light);
        remote.setCommand(0, lightOn, lightOff);

        Stereo stereo = new Stereo();
        StereoOnCommand stereoOn = new StereoOnCommand(stereo);
        StereoOffCommand stereoOff = new StereoOffCommand(stereo);
        remote.setCommand(1, stereoOn, stereoOff);

        Garage garage = new Garage();
        GarageOnCommand garageOn = new GarageOnCommand(garage);
        GarageOffCommand garageOff = new GarageOffCommand(garage);
        remote.setCommand(2, garageOn, garageOff);

        remote.onButtonWasPressed(0);
        remote.onButtonWasPressed(1);
        remote.onButtonWasPressed(2);
        remote.onButtonWasPressed(3);

        remote.offButtonWasPressed(0);
        remote.offButtonWasPressed(1);

        remote.undoButtonWasPushed();

        remote.toString();
        Assert.assertTrue("RemoteControl instance state error", remote.toString().equals("slot [0] LightOnCommand LightOffCommandslot [1] StereoOnCommand StereoOffCommandslot [2] GarageOnCommand GarageOffCommandslot [3] NoCommand NoCommandslot [4] NoCommand NoCommandslot [5] NoCommand NoCommandslot [6] NoCommand NoCommand"));
    }
}

package com.arpe.simon.designpatterncookbook;


import com.arpe.simon.designpatterncookbook.ObserverJava.CurrentConditionDisplay;
import com.arpe.simon.designpatterncookbook.ObserverJava.WeatherData;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 30/04/2015.
 */
public class ObserverJavaTest {

    @Test
    public void testObserver() {
        WeatherData weatherData = new WeatherData();

        weatherData.setMeasurements(7,17,27);

        CurrentConditionDisplay conditionDisplay = new CurrentConditionDisplay(weatherData);
        Assert.assertNotEquals("conditionDisplay object has been registered as observer after weatherData object has been updated", weatherData.getTemperature(), conditionDisplay.getTemperature());

        weatherData.setMeasurements(6,16,26);
        Assert.assertTrue("weatherData (Subject) humidity doesn't match with conditionData (Observer) humidity", weatherData.getHumidity()==conditionDisplay.getHumidity());
    }
}

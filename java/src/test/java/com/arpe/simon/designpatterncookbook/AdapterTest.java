package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.Adapter.Duck;
import com.arpe.simon.designpatterncookbook.Adapter.MallardDuck;
import com.arpe.simon.designpatterncookbook.Adapter.RealTurkey;
import com.arpe.simon.designpatterncookbook.Adapter.Turkey;
import com.arpe.simon.designpatterncookbook.Adapter.TurkeyAdapter;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 01/05/2015.
 */
public class AdapterTest {
    @Test
    public void testAdapter() {
        System.out.println("\nThe Adapter Pattern\n");

        Turkey realTurkey = new RealTurkey();
        realTurkey.gobble();
        realTurkey.fly();

        Duck mallardDuck = new MallardDuck();
        mallardDuck.quack();
        mallardDuck.fly();

        Duck turkey = new TurkeyAdapter(realTurkey);
        turkey.quack();
        turkey.fly();

        Assert.assertTrue(realTurkey instanceof RealTurkey && mallardDuck instanceof MallardDuck);
        Assert.assertTrue(turkey instanceof TurkeyAdapter);

    }
}

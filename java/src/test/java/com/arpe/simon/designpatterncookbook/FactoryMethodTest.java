package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.FactoryMethod.ChicagoPizzaStore;
import com.arpe.simon.designpatterncookbook.FactoryMethod.ChicagoStyleClamPizza;
import com.arpe.simon.designpatterncookbook.FactoryMethod.NYPizzaStore;
import com.arpe.simon.designpatterncookbook.FactoryMethod.NYStyleCheesePizza;
import com.arpe.simon.designpatterncookbook.FactoryMethod.Pizza;
import com.arpe.simon.designpatterncookbook.FactoryMethod.PizzaStore;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 30/04/2015.
 */
public class FactoryMethodTest {

    @Test
    public void testFactoryMethod() {
        System.out.println("\nFactory Method\n");
        Pizza pizza;
        PizzaStore nyStore = new NYPizzaStore();
        PizzaStore chicagoStore = new ChicagoPizzaStore();

        pizza = nyStore.orderPizza("cheese");
        System.out.println(String.format("First pizza ordered is a %s", pizza.getName()));

        Assert.assertTrue("NYStyleCheesePizza name doesn't match", pizza.getName().equals("NY Style Sauce and Cheese Pizza"));
        Assert.assertTrue("pizza object is not an instance of NYSTtyleCheesePizza", pizza instanceof NYStyleCheesePizza);

        pizza = chicagoStore.orderPizza("clam");
        System.out.println(String.format("Second pizza ordered is a %s", pizza.getName()));

        Assert.assertTrue("ChicagoStyleClamPizza name doesn't match", pizza.getName().equals("Chicago Style Sauce and Clam Pizza"));
        Assert.assertTrue("pizza object is not an instance of ChicagoStyleClamPizza", pizza instanceof ChicagoStyleClamPizza);


    }
}

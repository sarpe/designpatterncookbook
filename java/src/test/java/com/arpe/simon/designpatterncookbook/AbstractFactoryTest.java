package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.AbstractFactory.ChicagoPizzaStore;
import com.arpe.simon.designpatterncookbook.AbstractFactory.ClamPizza;
import com.arpe.simon.designpatterncookbook.AbstractFactory.NYPizzaStore;
import com.arpe.simon.designpatterncookbook.AbstractFactory.Pizza;
import com.arpe.simon.designpatterncookbook.AbstractFactory.PizzaStore;
import com.arpe.simon.designpatterncookbook.AbstractFactory.VeggiePizza;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 30/04/2015.
 */
public class AbstractFactoryTest {

    @Test
    public void testAbstractFactory() {


        System.out.println("\nAbstract Factory Pattern\n");
        Pizza pizza;
        PizzaStore pizzaStore;

        pizzaStore = new NYPizzaStore();
        pizza = pizzaStore.orderPizza("veggie");
        System.out.println(String.format("Ordering %s", pizza.getName()));

        Assert.assertTrue("NY Veggie Pizza name is wrong", "New York style Veggie pizza".equals(pizza.getName()));
        Assert.assertTrue("pizza object is not an instance of VeggiePizza", pizza instanceof VeggiePizza);

        pizza = pizzaStore.orderPizza("clam");
        System.out.println(String.format("Ordering %s", pizza.getName()));

        pizzaStore = new ChicagoPizzaStore();
        pizza = pizzaStore.orderPizza("veggie");
        System.out.println(String.format("Ordering %s", pizza.getName()));

        pizza = pizzaStore.orderPizza("clam");
        System.out.println(String.format("Ordering %s", pizza.getName()));

        Assert.assertTrue("Chicago Clams Pizza name is wrong", "Chicago style Clam pizza".equals(pizza.getName()));
        Assert.assertTrue("pizza object is not an instance of ClamPizza", pizza instanceof ClamPizza);

    }
}

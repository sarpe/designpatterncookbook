package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.SimpleFactory.Pizza;
import com.arpe.simon.designpatterncookbook.SimpleFactory.PizzaStore;
import com.arpe.simon.designpatterncookbook.SimpleFactory.SimplePizzaFactory;
import com.arpe.simon.designpatterncookbook.SimpleFactory.VeggiePizza;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 30/04/2015.
 */
public class SimpleFactoryTest {

    @Test
    public void testSimpleFactory() {
        System.out.println("\nSimple Factory Method\n");

        SimplePizzaFactory pizzaFactory = new SimplePizzaFactory();
        PizzaStore pizzaStore = new PizzaStore(pizzaFactory);

        Pizza veggiePizza = pizzaStore.orderPizza("veggie");
        System.out.println(String.format("Ordered a %s pizza", veggiePizza.getName()));

        boolean nameMatches = "Veggie".equals(veggiePizza.getName());
        boolean veggieInstance = veggiePizza instanceof VeggiePizza;

        Assert.assertTrue("Veggie Pizza name doesn't match ", nameMatches);
        Assert.assertTrue("veggiePizza object is not an instance of VeggiePizza", veggieInstance);

    }
}

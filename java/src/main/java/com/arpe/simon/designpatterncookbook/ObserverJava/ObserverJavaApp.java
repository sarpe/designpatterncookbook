package com.arpe.simon.designpatterncookbook.ObserverJava;

/**
 * Created by arpes on 30/04/2015.
 *
 * Observer Pattern defines a one-to-many dependency
 * between objects so that when one object changes state,
 * all of its dependents are notified and updated automatically.
 */
public class ObserverJavaApp {
    public ObserverJavaApp() {
        System.out.println("\nObserver Pattern\n");

        WeatherData weatherData = new WeatherData();

        CurrentConditionDisplay currentDisplay = new CurrentConditionDisplay(weatherData);

        weatherData.setMeasurements(80, 65, 30.4f);
        weatherData.setMeasurements(82, 70, 29.2f);
        weatherData.setMeasurements(78, 90, 29.2f);
    }
}

package com.arpe.simon.designpatterncookbook.Strategy;

/**
 * Created by arpes on 30/04/2015.
 */
public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Quack");
    }
}

package com.arpe.simon.designpatterncookbook.SimpleFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class PepperoniPizza extends Pizza {

    public PepperoniPizza() {
        name = "Pepperoni";
        dough = "Medium";
        sauce = "Spicy peppers";
    }
}

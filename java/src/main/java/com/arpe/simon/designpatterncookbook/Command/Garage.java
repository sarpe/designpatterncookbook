package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class Garage {

    public Garage() {

    }

    public void open() {

        System.out.println("Opening Garage Door");
    }

    public void enterPassCode() {
        System.out.println("Entering pass code 1234");
    }

    public void close() {
        System.out.println("Closing Garage Door");
    }

}

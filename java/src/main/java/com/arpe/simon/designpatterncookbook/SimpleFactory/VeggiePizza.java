package com.arpe.simon.designpatterncookbook.SimpleFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class VeggiePizza extends Pizza {

    public VeggiePizza() {
        name = "Veggie";
        dough = "Soft";
        sauce = "Maio";
    }
}

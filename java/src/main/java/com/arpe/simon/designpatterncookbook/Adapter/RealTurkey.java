package com.arpe.simon.designpatterncookbook.Adapter;

/**
 * Created by arpes on 01/05/2015.
 */
public class RealTurkey implements Turkey {
    @Override
    public void gobble() {
        System.out.println("Gobble!");
    }

    @Override
    public void fly() {
        System.out.println("I can fly a bit!");
    }
}

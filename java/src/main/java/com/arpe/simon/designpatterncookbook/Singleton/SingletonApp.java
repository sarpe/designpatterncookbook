package com.arpe.simon.designpatterncookbook.Singleton;

/**
 * Created by arpes on 01/05/2015.
 *
 * The Singleton Pattern ensures a class has only one instance,
 * and provide a global point of access to it.
 */
public class SingletonApp {
    public SingletonApp() {
        System.out.println("\nSingleton Pattern\n");

        Singleton single = Singleton.getInstance();
    }
}

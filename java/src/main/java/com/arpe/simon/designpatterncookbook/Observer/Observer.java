package com.arpe.simon.designpatterncookbook.Observer;

/**
 * Created by arpes on 30/04/2015.
 */
public interface Observer {
    void update(float temp, float humidity, float pressure);
}

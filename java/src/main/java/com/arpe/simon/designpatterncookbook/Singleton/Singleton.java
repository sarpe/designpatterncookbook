package com.arpe.simon.designpatterncookbook.Singleton;

/**
 * Created by arpes on 01/05/2015.
 */
public class Singleton {
    private static volatile Singleton INSTANCE;

    private Singleton() {

    }

    public static synchronized Singleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Singleton();
        }
        return INSTANCE;
    }
}

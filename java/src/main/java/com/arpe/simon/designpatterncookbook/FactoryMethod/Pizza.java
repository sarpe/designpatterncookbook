package com.arpe.simon.designpatterncookbook.FactoryMethod;

import java.util.ArrayList;

/**
 * Created by arpes on 29/04/2015.
 */
public abstract class Pizza {
    String name;
    String dough;
    String sauce;
    ArrayList<String> toppings = new ArrayList<>();

    void prepare() {
        System.out.println(String.format("Preparing %s", name));
        System.out.println("Tossing dough...");
        System.out.println("Adding sauce...");
        System.out.println("Adding toppings: ");
        for (int i = 0; i < toppings.size(); i++) {
            System.out.println(String.format(" %s", toppings.get(i)));
        }

    }

    void bake() {
        System.out.println("Bake for 30 minutes at 350");
    }

    void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }

    public String getName() {
        return name;
    }
}

package com.arpe.simon.designpatterncookbook.SimpleFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class CheesePizza extends Pizza {

    public CheesePizza() {
        name = "Cheese";
        dough = "Soft";
        sauce = "Mustard";
    }
}

package com.arpe.simon.designpatterncookbook.Strategy;

/**
 * Created by arpes on 30/04/2015.
 */
public interface QuackBehavior {
    void quack();
}

package com.arpe.simon.designpatterncookbook.Adapter;

/**
 * Created by arpes on 01/05/2015.
 *
 * The Adapter Pattern converts the interface of a class into
 * another interface the client expects. Adapter lets classes
 * work together that couldn't otherwise because of incompatible
 * interfaces.
 */
public class Adapter {

    public Adapter() {
        System.out.println("\nThe Adapter Pattern\n");

        Turkey realTurkey = new RealTurkey();
        Duck mallardDuck = new MallardDuck();
        screamAndFly(mallardDuck);
        Duck turkey = new TurkeyAdapter(realTurkey);
        screamAndFly(turkey);
    }

    private void screamAndFly(Duck duck) {
        duck.quack();
        duck.fly();
    }
}

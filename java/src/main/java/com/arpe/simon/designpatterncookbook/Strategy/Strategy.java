package com.arpe.simon.designpatterncookbook.Strategy;

/**
 * Created by arpes on 30/04/2015.
 *
 * The Strategy Pattern defines a family of algorithms,
 * encapsulates each one, and make them interchangeable.
 * Strategy lets the algorithm vary independently from
 * clients that use it.
 */
public class Strategy {
    public Strategy() {
        System.out.println("\nStrategy Pattern\n");

        Duck duck;
        duck = new MallardDuck();
        duckInteractor(duck);
        duck = new RedheadDuck();
        duckInteractor(duck);
        duck = new RubberDuck();
        duckInteractor(duck);
        duck = new DecoyDuck();
        duckInteractor(duck);
    }

    private void duckInteractor(Duck duck) {
        duck.display();
        duck.performFly();
        duck.performQuack();

    }

}

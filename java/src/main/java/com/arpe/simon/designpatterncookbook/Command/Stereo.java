package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class Stereo {

    public Stereo() {

    }

    public void setCd() {
        System.out.println("Choosing Cd to play");
    }

    public void setVolume() {
        System.out.println("Setup volume at 50%");
    }

    public void play() {
        System.out.println("Playing CD ...");
    }

    public void off() {
        System.out.println("Switching stereo off");
    }
}

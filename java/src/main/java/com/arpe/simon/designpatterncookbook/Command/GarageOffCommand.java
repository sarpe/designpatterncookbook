package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class GarageOffCommand implements Command {

    private Garage garage;

    public GarageOffCommand(Garage garage) {
        this.garage = garage;
    }
    @Override
    public void execute() {
        garage.close();
    }

    @Override
    public void undo() {
        garage.enterPassCode();
        garage.open();
    }
}

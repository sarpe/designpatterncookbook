package com.arpe.simon.designpatterncookbook.AbstractFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public interface PizzaIngredientFactory {
    Dough creatDough();
    Sauce createSauce();
    Cheese createCheese();
    Veggies[] createVeggies();
    Pepperoni createPepperoni();
    Clams createClams();
}

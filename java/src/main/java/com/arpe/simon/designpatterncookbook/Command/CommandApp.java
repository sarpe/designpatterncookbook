package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 *
 * The Command Pattern encapsulates a request as an
 * object, thereby letting you parametrize other objects
 * with different requests, queue or log request, and support
 * undoable operations.
 */
public class CommandApp {
    public CommandApp() {
        System.out.print("\nCommand Pattern\n");

        RemoteControl remote = new RemoteControl();

        Light light = new Light();
        LightOnCommand lightOn = new LightOnCommand(light);
        LightOffCommand lightOff = new LightOffCommand(light);
        remote.setCommand(0, lightOn, lightOff);

        Stereo stereo = new Stereo();
        StereoOnCommand stereoOn = new StereoOnCommand(stereo);
        StereoOffCommand stereoOff = new StereoOffCommand(stereo);
        remote.setCommand(1, stereoOn, stereoOff);

        Garage garage = new Garage();
        GarageOnCommand garageOn = new GarageOnCommand(garage);
        GarageOffCommand garageOff = new GarageOffCommand(garage);
        remote.setCommand(2, garageOn, garageOff);

        remote.onButtonWasPressed(0);
        remote.onButtonWasPressed(1);
        remote.onButtonWasPressed(2);
        remote.onButtonWasPressed(3);

        remote.offButtonWasPressed(0);
        remote.offButtonWasPressed(1);

        remote.undoButtonWasPushed();
    }
}

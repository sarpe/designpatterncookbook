package com.arpe.simon.designpatterncookbook.FactoryMethod;

/**
 * Created by arpes on 29/04/2015.
 */
public class FactoryMethod {

    public FactoryMethod() {

        System.out.println("\nFactory Method\n");
        Pizza pizza;
        PizzaStore nyStore = new NYPizzaStore();
        PizzaStore chicagoStore = new ChicagoPizzaStore();

        pizza = nyStore.orderPizza("cheese");
        System.out.println(String.format("First pizza ordered is a %s", pizza.getName()));

        pizza = chicagoStore.orderPizza("clam");
        System.out.println(String.format("Second pizza ordered is a %s", pizza.getName()));

    }
}

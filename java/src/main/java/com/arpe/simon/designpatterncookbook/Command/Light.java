package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class Light {
    public Light() {

    }

    public void on() {
        System.out.println("Turning Light on...");
    }

    public void off() {
        System.out.println("Turning Light off...");
    }
}

package com.arpe.simon.designpatterncookbook.Decorator;

/**
 * Created by arpes on 30/04/2015.
 */
public abstract class Beverage {
    String description = "Unknown Beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}

package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class RemoteControl {
    Command[] onCommands;
    Command[] offCommands;
    Command undo;

    public RemoteControl() {
        onCommands = new Command[7];
        offCommands = new Command[7];

        Command noCommand = new NoCommand();
        for(int i = 0; i < 7 ; i++) {
            onCommands[i] = noCommand;
            offCommands[i] = noCommand;
        }
    }

    public void setCommand(int slot, Command on, Command off) {
        onCommands[slot] = on;
        offCommands[slot] = off;
    }

    public void onButtonWasPressed(int slot) {
        onCommands[slot].execute();
        undo = onCommands[slot];
    }

    public void offButtonWasPressed(int slot) {
        offCommands[slot].execute();
        undo = offCommands[slot];
    }

    public void undoButtonWasPushed() {
        undo.undo();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < onCommands.length; i++) {
            sb.append("slot [" + i + "] " + onCommands[i].getClass().getSimpleName() + " " +
                    offCommands[i].getClass().getSimpleName());
        }

        return sb.toString();
    }
}

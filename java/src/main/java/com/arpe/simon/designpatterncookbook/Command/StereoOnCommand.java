package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class StereoOnCommand implements Command {

    private Stereo stereo;

    public StereoOnCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        stereo.setCd();
        stereo.setVolume();
        stereo.play();
    }

    @Override
    public void undo() {
        stereo.off();

    }
}

package com.arpe.simon.designpatterncookbook.FactoryMethod;

/**
 * Created by arpes on 29/04/2015.
 */
public class NYPizzaStore extends PizzaStore {

    private static final String CHEESE = "cheese";
    private static final String PEPPERONI = "pepperoni";
    private static final String CLAM = "clam";
    private static final String VEGGIE = "veggie";

    @Override
    protected Pizza createPizza(String type) {
            Pizza pizza = null;
            if (type.equals(CHEESE)) {
                pizza = new NYStyleCheesePizza();
            } else if (type.equals(PEPPERONI)) {
                pizza = new NYStylePepperoniPizza();
            } else if (type.equals(CLAM)) {
                pizza = new NYStyleClamPizza();
            } else if (type.equals(VEGGIE)) {
                pizza = new NYStyleVeggiePizza();
            }
            return pizza;
    }
}


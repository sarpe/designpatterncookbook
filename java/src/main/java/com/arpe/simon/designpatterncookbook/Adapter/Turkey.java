package com.arpe.simon.designpatterncookbook.Adapter;

/**
 * Created by arpes on 01/05/2015.
 */
public interface Turkey {
    void gobble();
    void fly();
}

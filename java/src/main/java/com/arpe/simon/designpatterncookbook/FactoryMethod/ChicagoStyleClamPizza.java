package com.arpe.simon.designpatterncookbook.FactoryMethod;

/**
 * Created by arpes on 29/04/2015.
 */
public class ChicagoStyleClamPizza extends Pizza {
    public ChicagoStyleClamPizza() {
        name = "Chicago Style Sauce and Clam Pizza";
        dough = "Thick Crust Dough";
        sauce = "Lemon Sauce";

        toppings.add("Eggs");
    }

}

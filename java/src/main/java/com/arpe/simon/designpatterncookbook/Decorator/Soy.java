package com.arpe.simon.designpatterncookbook.Decorator;

/**
 * Created by arpes on 30/04/2015.
 */
public class Soy extends CondimentDecorator {
    private Beverage beverage;
    public Soy(Beverage beverage) {
        this.beverage = beverage;
    }
    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Soy";
    }

    @Override
    public double cost() {
        return 0.59 + beverage.cost();
    }
}

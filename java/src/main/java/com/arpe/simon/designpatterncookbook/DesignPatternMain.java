package com.arpe.simon.designpatterncookbook;

import com.arpe.simon.designpatterncookbook.AbstractFactory.AbstractFactory;
import com.arpe.simon.designpatterncookbook.Adapter.Adapter;
import com.arpe.simon.designpatterncookbook.Command.CommandApp;
import com.arpe.simon.designpatterncookbook.Decorator.Decorator;
import com.arpe.simon.designpatterncookbook.FactoryMethod.FactoryMethod;
import com.arpe.simon.designpatterncookbook.Observer.ObserverApp;
import com.arpe.simon.designpatterncookbook.ObserverJava.ObserverJavaApp;
import com.arpe.simon.designpatterncookbook.SimpleFactory.SimpleFactory;
import com.arpe.simon.designpatterncookbook.Singleton.SingletonApp;
import com.arpe.simon.designpatterncookbook.Strategy.Strategy;

public class DesignPatternMain {

    public static void main(String[] argv) {

        System.out.println("\nHello Design Pattern World\n");

        new SimpleFactory();
        new FactoryMethod();
        new AbstractFactory();
        new Strategy();
        new ObserverApp();
        new ObserverJavaApp();
        new Decorator();
        new SingletonApp();
        new CommandApp();
        new Adapter();

    }
}

package com.arpe.simon.designpatterncookbook.Strategy;

/**
 * Created by arpes on 30/04/2015.
 */
public class MallardDuck extends Duck {

    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard Duck");
    }
}

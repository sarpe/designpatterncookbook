package com.arpe.simon.designpatterncookbook.SimpleFactory;

/**
* Created by arpes on 29/04/2015.
*
* The Simple Factory isn't actually a Design Pattern;
* it's more a programming idiom. But it is commonly used,
* so we'll give it a Head First Pattern Honorable Mention.
* */

 public class SimplePizzaFactory {

    private static final String CHEESE = "cheese";
    private static final String PEPPERONI = "pepperoni";
    private static final String CLAM = "clam";
    private static final String VEGGIE = "veggie";

    public Pizza createPizza(String type) {
        Pizza pizza = null;
        if (type.equals(CHEESE)) {
            pizza = new CheesePizza();
        } else if (type.equals(PEPPERONI)) {
            pizza = new PepperoniPizza();
        } else if (type.equals(CLAM)) {
            pizza = new ClamPizza();
        } else if (type.equals(VEGGIE)) {
            pizza = new VeggiePizza();
        }
        return pizza;
    }
}

package com.arpe.simon.designpatterncookbook.SimpleFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class SimpleFactory {

    public SimpleFactory() {
        System.out.println("\nSimple Factory Method\n");

        SimplePizzaFactory pizzaFactory = new SimplePizzaFactory();
        PizzaStore pizzaStore = new PizzaStore(pizzaFactory);

        Pizza veggiePizza = pizzaStore.orderPizza("veggie");
        System.out.println(String.format("Ordered a %s pizza", veggiePizza.getName()));
    }
}

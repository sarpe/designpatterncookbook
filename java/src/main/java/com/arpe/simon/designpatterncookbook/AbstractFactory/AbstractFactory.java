package com.arpe.simon.designpatterncookbook.AbstractFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class AbstractFactory {

    public AbstractFactory() {

        System.out.println("\nAbstract Factory Pattern\n");
        Pizza pizza;
        PizzaStore pizzaStore;

        pizzaStore = new NYPizzaStore();
        pizza = pizzaStore.orderPizza("veggie");
        System.out.println(String.format("Ordering %s", pizza.getName()));
        pizza = pizzaStore.orderPizza("clam");
        System.out.println(String.format("Ordering %s", pizza.getName()));

        pizzaStore = new ChicagoPizzaStore();
        pizza = pizzaStore.orderPizza("veggie");
        System.out.println(String.format("Ordering %s", pizza.getName()));
        pizza = pizzaStore.orderPizza("clam");
        System.out.println(String.format("Ordering %s", pizza.getName()));
    }
}

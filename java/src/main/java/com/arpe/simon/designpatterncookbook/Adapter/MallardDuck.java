package com.arpe.simon.designpatterncookbook.Adapter;

/**
 * Created by arpes on 01/05/2015.
 */
public class MallardDuck implements Duck {

    @Override
    public void quack() {
    System.out.println("Quack");
    }

    @Override
    public void fly() {
        System.out.println("I can fly!");
    }
}

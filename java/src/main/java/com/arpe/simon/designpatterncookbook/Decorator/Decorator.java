package com.arpe.simon.designpatterncookbook.Decorator;

/**
 * Created by arpes on 30/04/2015.
 *
 * The Decorator Pattern attaches additional
 * responsibilities to an object dynamically.
 * Decorators provide a flexible alternative to
 * subclassing for extending functionality.
 */
public class Decorator {
    public Decorator() {
        System.out.println("\nDecorator Pattern\n");
        Beverage beverage = new Espresso();
        System.out.println(String.format("%s $ %.2f", beverage.getDescription(), beverage.cost()));

        Beverage beverage1 = new HouseBlend();
        beverage1 = new Mocha(beverage1);
        beverage1 = new Whip(beverage1);
        System.out.println(String.format("%s $ %.2f", beverage1.getDescription(), beverage1.cost()));

    }
}

package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class GarageOnCommand implements Command {

    private Garage garage;

    public GarageOnCommand(Garage garage) {
        this.garage = garage;
    }
    @Override
    public void execute() {
        garage.enterPassCode();
        garage.open();
    }

    @Override
    public void undo() {
        garage.close();
    }
}

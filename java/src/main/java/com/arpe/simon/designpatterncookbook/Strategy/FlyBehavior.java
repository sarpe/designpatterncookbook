package com.arpe.simon.designpatterncookbook.Strategy;

/**
 * Created by arpes on 30/04/2015.
 */
public interface FlyBehavior {
    void fly();
}

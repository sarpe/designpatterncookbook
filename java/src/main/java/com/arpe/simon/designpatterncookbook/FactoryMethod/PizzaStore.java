package com.arpe.simon.designpatterncookbook.FactoryMethod;

/**
 * Created by arpes on 29/04/2015.
 */
public abstract class PizzaStore {

    public Pizza orderPizza(String type) {
        Pizza pizza;
        pizza = createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    protected abstract Pizza createPizza(String type);

    // other methods here...
}

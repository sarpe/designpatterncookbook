package com.arpe.simon.designpatterncookbook.Decorator;

/**
 * Created by arpes on 30/04/2015.
 */
public abstract class CondimentDecorator extends Beverage {

    public abstract String getDescription();
}

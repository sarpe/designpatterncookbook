package com.arpe.simon.designpatterncookbook.SimpleFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class ClamPizza extends Pizza {

    public ClamPizza() {
        name = "Claim";
        dough = "Crispy";
        sauce = "Sour cream";
    }
}

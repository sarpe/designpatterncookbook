package com.arpe.simon.designpatterncookbook.AbstractFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class VeggiePizza extends Pizza {

    PizzaIngredientFactory pizzaIngredientFactory;
    public VeggiePizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.pizzaIngredientFactory = pizzaIngredientFactory;
    }

    @Override
    void prepare() {
        System.out.println(String.format("Preparing %s", name));
        veggies = pizzaIngredientFactory.createVeggies();
        sauce = pizzaIngredientFactory.createSauce();
        cheese = pizzaIngredientFactory.createCheese();
    }
}

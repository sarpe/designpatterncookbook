package com.arpe.simon.designpatterncookbook.AbstractFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class CheesePizza extends Pizza {
    PizzaIngredientFactory pizzaIngredientFactory;

    public CheesePizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.pizzaIngredientFactory = pizzaIngredientFactory;
    }

    @Override
    void prepare() {
        System.out.println(String.format("Preparing %s", name));
        dough = pizzaIngredientFactory.creatDough();
        sauce = pizzaIngredientFactory.createSauce();
        cheese = pizzaIngredientFactory.createCheese();
    }
}

package com.arpe.simon.designpatterncookbook.Strategy;

/**
 * Created by arpes on 30/04/2015.
 */
public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I can't fly!");
    }
}

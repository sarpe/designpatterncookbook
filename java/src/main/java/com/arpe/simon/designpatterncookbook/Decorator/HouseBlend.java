package com.arpe.simon.designpatterncookbook.Decorator;

/**
 * Created by arpes on 30/04/2015.
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "House Blend Coffee";
    }

    @Override
    public double cost() {
        return 1.99;
    }
}

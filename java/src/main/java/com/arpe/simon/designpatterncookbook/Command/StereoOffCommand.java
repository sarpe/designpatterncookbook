package com.arpe.simon.designpatterncookbook.Command;

/**
 * Created by arpes on 01/05/2015.
 */
public class StereoOffCommand implements Command {

    private Stereo stereo;

    public StereoOffCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        stereo.off();
    }

    @Override
    public void undo() {
        stereo.setCd();
        stereo.setVolume();
        stereo.play();
    }
}

package com.arpe.simon.designpatterncookbook.ObserverJava;

/**
 * Created by arpes on 30/04/2015.
 */
public interface DisplayElement {
    void display();
}

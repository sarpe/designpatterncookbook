package com.arpe.simon.designpatterncookbook.Observer;

/**
 * Created by arpes on 30/04/2015.
 */
public interface Subject {
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers();
}

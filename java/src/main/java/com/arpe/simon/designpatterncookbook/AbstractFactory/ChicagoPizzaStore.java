package com.arpe.simon.designpatterncookbook.AbstractFactory;

/**
 * Created by arpes on 29/04/2015.
 */
public class ChicagoPizzaStore extends PizzaStore {

    private static final String CHEESE = "cheese";
    private static final String VEGGIE = "veggie";
    private static final String CLAM = "clam";
    private static final String PEPPERONI = "pepperoni";
    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza;
        PizzaIngredientFactory pizzaIngredientFactory = new ChicagoPizzaIngredientFactory();

        switch (type) {
            case CHEESE:
                pizza = new CheesePizza(pizzaIngredientFactory);
                pizza.setName("Chicago style Cheese pizza");
                break;
            case VEGGIE:
                pizza = new VeggiePizza(pizzaIngredientFactory);
                pizza.setName("Chicago style Veggie pizza");
                break;
            case CLAM:
                pizza = new ClamPizza(pizzaIngredientFactory);
                pizza.setName("Chicago style Clam pizza");
                break;
            case PEPPERONI:
                pizza = new PepperoniPizza(pizzaIngredientFactory);
                pizza.setName("Chicago style Pepperoni pizza");
                break;
            default:
                throw new IllegalStateException("illegal state for " + type);
        }

        return pizza;
    }
}

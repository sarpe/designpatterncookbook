package com.arpe.simon.designpatterncookbook.ObserverJava;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by arpes on 30/04/2015.
 */
public class WeatherData extends Observable {

    private float temperature, humidity, pressure;

    public WeatherData() {
    }

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temp, float humidity, float pressure) {
        this.temperature = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }
}

# README #

Project containing most commonly used design pattern from GoF book.
Shamelessly inspired from the book "Design Patterns" by Freeman & Freeman
Project Gradle compatible + tests

Some design patterns are:

 - AbstractFactory

 - Adapter

 - Command

 - Decorator

 - FactoryMethod

 - Observer

 - ObserverJava

 - SimpleFactory

 - Singleton

 - Strategy



### What is this repository for? ###

Students,
 
Academic research,

Job interview preparation,




Pull requests are welcome :)


License
-------

    Copyright (C) 2015 Simone Arpe
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.